
{script src="js/tygh/tabs.js"}
{literal}
    <script type="text/javascript">
    function fn_check_option_type(value, tag_id)
    {
        var id = tag_id.replace('option_type_', '').replace('elm_', '');
        Tygh.$('#tab_option_variants_' + id).toggleBy(!(value == 'S' || value == 'R' || value == 'C'));
        Tygh.$('#required_options_' + id).toggleBy(!(value == 'I' || value == 'T' || value == 'F'));
        Tygh.$('#extra_options_' + id).toggleBy(!(value == 'I' || value == 'T'));
        Tygh.$('#file_options_' + id).toggleBy(!(value == 'F'));

        if (value == 'C') {
            var t = Tygh.$('table', '#content_tab_option_variants_' + id);
            Tygh.$('.cm-non-cb', t).switchAvailability(true); // hide obsolete columns
            Tygh.$('tbody:gt(1)', t).switchAvailability(true); // hide obsolete rows

        } else if (value == 'S' || value == 'R') {
            var t = Tygh.$('table', '#content_tab_option_variants_' + id);
            Tygh.$('.cm-non-cb', t).switchAvailability(false); // show all columns
            Tygh.$('tbody', t).switchAvailability(false); // show all rows
            Tygh.$('#box_add_variant_' + id).show(); // show "add new variants" box

        } else if (value == 'I' || value == 'T') {
            Tygh.$('#extra_options_' + id).show(); // show "add new variants" box
        }
    }
    </script>
{/literal}

{capture name="mainbox"}
 <h3>Здесь будет страница с сотрудниками</h3>
 <p>{$tst_msg}</p>

    {if $object == "global"}
        {assign var="select_languages" value=true}
        {assign var="delete_target_id" value="pagination_contents"}
    {else}
        {assign var="delete_target_id" value="product_options_list"}
    {/if}

    {include file="common/pagination.tpl"}

    {if !($runtime.company_id && $product_data.shared_product == "Y" && $runtime.company_id != $product_data.company_id)}
        {capture name="toolbar"}
            {capture name="add_new_picker"}
                {if $product_data}
                    {include file="views/product_options/update.tpl" option_id="0" company_id=$product_data.company_id disable_company_picker=true}
                {else}
                    {include file="views/product_options/update.tpl" option_id="0"}
                {/if}
            {/capture}
            {if $object == "product"}
                {assign var="position" value="pull-right"}
            {/if}
            {if $view_mode == "embed"}
                {include file="common/popupbox.tpl" id="add_new_option" text=__("new_option") link_text=__("add_option") act="general" content=$smarty.capture.add_new_picker meta=$position icon="icon-plus"}

            {else}
                {include file="common/popupbox.tpl" id="add_new_option" text=__("new_option") title=__("add_option") act="general" content=$smarty.capture.add_new_picker meta=$position icon="icon-plus"}
            {/if}

        {/capture}
        {$extra nofilter}
    {/if}
        {if $object != "global"}
            <div class="btn-toolbar clearfix cm-toggle-button">
                {$smarty.capture.toolbar nofilter}
            </div>
        {else}
            {capture name="buttons"}
                {if $product_options && $object == "global"}
                    {include file="buttons/button.tpl" but_text=__("apply_to_products") but_role="action" but_href="product_options.apply"}
                {/if}
            {/capture}
            {capture name="adv_buttons"}
                {$smarty.capture.toolbar nofilter}
            {/capture}
        {/if}

        <div class="items-container" id="product_options_list">
            {if $product_options}
            {else}
                <p class="no-items">{__("no_data")}</p>
            {/if}
            <!--product_options_list--></div>
    {include file="common/pagination.tpl"}

{/capture}

{if $object == "staff"}
    {$smarty.capture.mainbox nofilter}
{else}

    {include file="common/mainbox.tpl" title=__("options") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_language=$select_language}
{/if}




