<?php

use Tygh\Registry;

$schema['central']['website']['items']['staff'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),    
    'href' => 'staff.manage',
    'alt' => 'staff.update',
    'position' => 1000
);


return $schema;
