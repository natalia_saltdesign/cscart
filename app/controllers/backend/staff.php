<?php


use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_define('KEEP_UPLOADED_FILES', true);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $suffix = '';


    if ($mode == 'update') {

        $suffix = ".manage";
    }

    if ($mode == 'delete') {

        $suffix = ".manage";
    }



    return array(CONTROLLER_STATUS_OK, 'staff' . $suffix);
}

if ($mode == 'manage') {
    $params = $_REQUEST;

  $tst=fn_get_employees();

/*
    list($product_options, $search) = fn_get_employees($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);
*/ 
    Tygh::$app['view']->assign('product_options', $product_options);

    Tygh::$app['view']->assign('tst_msg', $tst);
    Tygh::$app['view']->assign('search', $search);
    Tygh::$app['view']->assign('object', 'global');

    if (empty($product_options) && defined('AJAX_REQUEST')) {
        $ajax->assign('force_redirection', fn_url('product_options.manage'));
    }

//
// Apply options to products
//
} elseif ($mode == 'apply') {

    list($product_options, $search) = fn_get_product_global_options();

    Tygh::$app['view']->assign('product_options', $product_options);

//
// Update option
//
} elseif ($mode == 'update') {

    $product_id = !empty($_REQUEST['product_id']) ? $_REQUEST['product_id'] : 0;

    $o_data = fn_get_product_option_data($_REQUEST['option_id'], $product_id);

    if (fn_allowed_for('ULTIMATE') && !empty($_REQUEST['product_id'])) {
        Tygh::$app['view']->assign('shared_product', fn_ult_is_shared_product($_REQUEST['product_id']));
        Tygh::$app['view']->assign('product_company_id', db_get_field('SELECT company_id FROM ?:products WHERE product_id = ?i', $_REQUEST['product_id']));
    }

    if (isset($_REQUEST['object'])) {
        Tygh::$app['view']->assign('object', $_REQUEST['object']);
    }
    Tygh::$app['view']->assign('option_data', $o_data);
    Tygh::$app['view']->assign('option_id', $_REQUEST['option_id']);

}

/*
if (!empty($_REQUEST['product_id'])) {
    Tygh::$app['view']->assign('product_id', $_REQUEST['product_id']);
}
*/

?>